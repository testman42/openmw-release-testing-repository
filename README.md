# What is this?

This repository is a hub where we can discuss and write down all everything related to OpenMW 0.47 release candidates.

# Disclaimer

This is a community-driven effort, not directly affiliated with the OpenMW project.

# How this works:

Each notable entry in 0.47 changelog should be tested.
We are using GitLab issues, as they are very fitting format for reports and discussions about individual changes.
There is a **[list of tests to be performed](https://gitlab.com/sjek/openmw-release-testing-repository/-/issues)** in order to see if there are any problems with any of the new features.

Consider joining Some Casual Openmw Testing Server if you wish to be more actively involved:
- SCOTS: https://discord.gg/5TsMmw3 

## How to test

* Download the [latest 0.47  Release Candidate](https://forum.openmw.org/viewtopic.php?p=70914#p70914).
    * It is strongly recommended to have OpenMW 0.4**6** release installed as well, so that you can make comparisons between the two versions.
* Pick a feature that you wish to test from the list that is linked above
    * Each feature has instructions for how to most easily perform the test.
    * If the test passess without any problems, then try to take a different approach and think outside of the box about how you could get the feature to not work properly
* If you encounter unexpected behaviour / issues / bugs, then make sure that you can replicate the issue.
    * Repeat the process at least 4 or 5 times, make sure that problem happens each time
    * Write down the process for how to replicate the issue, include absolutely all details.

Or just play around with the game and try to break stuff.

Here in this repo and in SCOTS you can feel free to report **anything** that you think could be considered a problem.
Maybe you find something that will turn out to be an actual problem that will be worthy of OpenMW developer's attention.

## Reporting your findings

Firstly read through the existing reports. If there is an existing report that already describes **exactly** what you found, then you don't need to post a duplicate of that, you can just react with 👍 below the post.

Otherwise please submit your own report. Information that you should include in that:
* Platform / operating system that you tested on
* Which RC you tested (at the time of writing, latest one was RC**2**.)
* Your findings
* Any additional information that you think are relevant to your report

## Making new issues

If you are testing something that does not have an issue yet, then feel free to create new one.

As written and for keeping things consistent the issue´s name is copied from
changelog. Would refer issue´s structure like:

        (if relevant, Link to the original issue from OpenMW Gitlab repository)

        Effect:

            (What is the cause / affected behaviour)
            (ie. old and new or some other way making things clear)

        Test:

            (Made or need to made tests and way of testing)


# Relevant Links

Forum thread for OpenMW 0.47:
- https://forum.openmw.org/viewtopic.php?f=20&t=6981

Some Casual Openmw Testing Server Discord:
- https://discord.gg/5TsMmw3 
 
For now for simplicity and ability to modify on tablet, google doc is used for tracking written issues
- https://docs.google.com/document/d/1fG7pw2E1xXiQICDeyq3zzUDPHDI6ECC_OuPtCuzJd5M/edit#

Original 0.47 test doc (not much but as a small refernce of idea)
- https://docs.google.com/document/d/1pLeuwbDOaEs3KsPdhWtRAPazGqv94SailTZQ5q7BPcY/edit#
